/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* Función que ejecuta sonido de la nota */
play = function play(m) {
   var audio = document.getElementById(m).innerHTML = "<embed src='src/notas/" + m + ".mp3' hidden=true>";
};

/*LECTOR DE NOTAS*/

function validarString() {

   var cadena = document.getElementById("cadena").value;
   var array = cadena.split("");

   //Funcion setInterval que ejecuta los ciclos cada segundo   
   var n = 0;
   setInterval(function () {

      /*Blancas*/
      if (array[n] == "a" || array[n] == "A") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/do.mp3'hidden=true>";
      }

      if (array[n] == "s" || array[n] == "S") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/re.mp3'hidden=true>";
      }

      if (array[n] == "d" || array[n] == "N") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/mi.mp3'hidden=true>";
      }

      if (array[n] == "f" || array[n] == "F") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/fa.mp3'hidden=true>";
      }
      if (array[n] == "g" || array[n] == "G") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/sol.mp3'hidden=true>";
      }

      if (array[n] == "h" || array[n] == "H") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/la.mp3'hidden=true>";
      }

      if (array[n] == "j" || array[n] == "J") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/si.mp3'hidden=true>";
      }

      if (array[n] == "k" || array[n] == "K") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/du.mp3'hidden=true>";
      }

      /*Negras*/

      if (array[n] == "w" || array[n] == "W") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/dom.mp3'hidden=true>";
      }

      if (array[n] == "e" || array[n] == "E") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/rem.mp3'hidden=true>";
      }

      if (array[n] == "t" || array[n] == "T") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/fam.mp3'hidden=true>";
      }

      if (array[n] == "y" || array[n] == "Y") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/solm.mp3'hidden=true>";
      }
      if (array[n] == "u" || array[n] == "U") {
         document.getElementById("do").innerHTML = "<embed src='src/notas/lam.mp3'hidden=true>";
      }

      n++;
   }, 500);
}

/***/ })
/******/ ]);
var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: path.join(__dirname, './src'),
    output: {
        path: path.resolve(__dirname, './lib'),
        filename: 'bundle.js'
    },  
    resolve: {
    extensions: ['.js']
  },
    
module: {
    loaders: [{
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: '/node_modules',
        query: {
        presets: ['es2015']}
    }]
  }
};
# Taller Desarrollo Web
## (Parte tórica)

**1)** Arropados Ltda.

  - **Diseño:**
  Se manejarán conceptos visuales que logren expresar la identidad de la empresa, tomando la información necesaria de su portafolio de servicios, tales como misión, visión, logotipos y demás.
Posible sitio similar: Tienda onlina ADIDAS.
  
  - **Web Layout:** De forma general el Web Layout a usar puede ser simiilar para esta empresa que desea implementar su tienda online, es recomendable un sitio donde primen las imágenes de los productos, ya que no interviene la inversión a realizar.
  
  - **Tecnologías:** Para el desarrollo de la página web se hará uso de tecnologías que permiten un óptimo desempeño por parte del programador en cuanto a escritura de código se refiere, dando la posibilidad de ahorrar tiempo y ajustando la funcionalidad de la página para cualquier buscador. Por lo tanto se utilizará babel, para el acoplamiento; por otro lado Html5 y Css con ayuda de Sass, que proporciona diversas ventajas al momento de aplicar los respectivos estilos. Para la adaptación de la página web a dispositivos móviles, se aplicarán propiedades de css como Flexbox y Grid.

**2)** Surrender20 Co.

Para el desearrollo de esta página web se hará uso de un diseño básico, teniendo en cuenta que la inversión no debe ser muy elevada, adicional a eso se aplicarán algunas tecnologías diferentes a las básicas que permitan una cómoda vista de la web a través de tabletas, tales como flex-box. El uso de imágenes será limitado.

**¿Qué es un Polyfill?**
Básicamente es un trozo de código que permite hacer uso de nuevas funciones que en muchos casos no soportan los navegadores de forma nativa, proporcionando al programador la posibilidad de trabajar con los avances en programación sobre cualquier navegador haciendo uso de Polyfill.

Ejemplo:
Un polyfill para la funcionalidad de WebSockets podría crear una variable global llamada window.WebSocket con las mismas propiedades y métodos que la implementación nativa de WebSockets. De esta manera, podríamos escribir nuestro propio código dentro de la página haciendo llamadas a WebSockets sin preocuparnos de si el navegador los soporta o no.
Recuperado de: https://www.campusmvp.es/recursos/post/modernizr-como-sustituir-funcionalidad-no-soportada-con-polyfills-parte-2.aspx

### Definición de conceptos:

**Compilar:** En esencia el término compilar hace referencia a la traducción de un lenguaje de programación de alto nivel al lenguaje de la máquina.

**Transpilar:** El término transpilar, a diferencia de compilar, tiene como objetivo transformar un código de un lenguaje de programación a otro, generalmente usado para lograr ejecutar el código en diferentes browsers y versiones.

**Interpretar:** Tiene bastante similaridad con el termino Compilar, sin embargo la diferencia radica en que el intérprete tiene la necesidad de entender las sentencias en tiempo rea y por ende en algunos casos suele ser más lenta su ejecución, a diferencia del compilador.

**Procesar:** Es la obtención de datos, con el fin de analizarlos y extraer información importante, que posteriormente será la base para la toma de decisiones. Trata de brindar una sintaxis más lejible para el usuario de un lenguaje de programación. 


### COMPILAR 
| ventajas | Desventajas |
| ------ | ------ |
| Autonomia | No interpreta en tiempo real |
| Velocidad | No se adapta a todos los entornos |


### TRANSPILAR
| ventajas | Desventajas |
| ------ | ------ |
| Adaptación al entorno | Se requiere de dependencias y archivos externos |

### INTERPRETAR 
| ventajas | Desventajas |
| ------ | ------ |
| Inferencia | Más lento que compilar |
| Interpretación en tiempo real  |  |

### PROCESAR 
| ventajas | Desventajas |
| ------ | ------ |
| Lenguaje Legible y agradable | Poca adaptación |

### ¿Qué es bundler?
Es una herramienta que proporciona un mejor entorno para aquellos que trabajan con Ruby, todo esto a través de la instalación de gemas exactas y versiones.

### ¿Qué tienen en común npm, bower y composer?
Trabajan y gestionan las dependencias de diferentes proyectos, permitiendo una correcta compilación y ejecución de diversos módulos necesarios en el desarrollo.
